lincolnplots
============

Helper functions for plotting figures in R for my dissertation.

***
[Matthew Lincoln](http://matthewlincoln.net) | University of Maryland
